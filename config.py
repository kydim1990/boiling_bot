PROXY_LIST = ['http://free-proxy-list.net']
URL = 'http://lifestories17.ru/'

DEVICES = [(1024, 768), (1280, 800), (1360, 768), (1366, 768), (1440, 900), (640, 1136), (480, 720), (360,480),
           (1600, 900), (1680, 1050), (1920, 1080), (2560, 1440), (320, 480), (640, 960), (540, 960), (480, 800)]


USER_AGENTS = [
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/53 (KHTML, like Gecko) Chrome/15.0.87",
    "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko)"
    " Version/10.0 Mobile/14E304 Safari/602.1",
    "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko)"
    " Version/4.0 Mobile Safari/534.30",
    "Mozilla/5.0 (Linux; Android 7.0; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko)"
    " Chrome/59.0.3071.125 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 7.0; SM-A310F Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko)"
    " Chrome/55.0.2883.91 Mobile Safari/537.36 OPR/42.7.2246.114996",
    'Opera/9.80 (Android 4.1.2; Linux; Opera Mobi/ADR-1305251841) Presto/2.11.355 Version/12.10',
]