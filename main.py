from web_driver import Webdriver
from utils import load_proxy, getallproxy
from config import URL
from time import sleep
from random import random
import traceback
import logging
from selenium.common.exceptions import TimeoutException
from proxy import Proxy

logging.basicConfig(filename='main.log', format='%(asctime)s %(levelname)-8s %(message)s', level=logging.DEBUG,
                    datefmt='%Y-%m-%d %H:%M:%S')


if __name__ == '__main__':
    while True:
        pl = getallproxy()
        proxy = Proxy()
        for page in pl:
            for i, p in enumerate(page):
                print i, p
                if not proxy.check(newproxy=p):
                    continue
                try:
                    wb = Webdriver(proxy_http=p)
                    wb.driver.get(URL)
                    rand = random() + (random() + 1) * 10 + 5
                    print "sleep {0}".format(rand)
                    sleep(rand)
                    print 'ok!'
                    proxy.add_proxy(p)
                    logging.info('Good_proxy {0}'.format(p))
                except TimeoutException as e:
                    print 'error timeout'
                    proxy.add_proxy(p, 'bad')
                    logging.error('timeout {0}'.format(p))
                except:
                    print traceback.print_exc()
                    logging.error(traceback.print_exc())
                finally:
                    wb.kill()
            proxy.save()
