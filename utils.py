# -*- coding: latin-1 -*-
import requests
from bs4 import BeautifulSoup
from config import PROXY_LIST
import os
import pickle


session = requests.Session()
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko)'
                         ' Chrome/39.0.2171.95 Safari/537.36'}


def get_url_from_tbody(tbody):
    url_for_test = 'https://www.python.org'
    res = []
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko)'
                             ' Chrome/39.0.2171.95 Safari/537.36',
               }
    trs = tbody.find_all("tr")
    for i, t in enumerate(trs):
        td = t.find_all("td")
        if len(td) < 2:
            continue

        is_https = True if td[-2].text == 'yes' else False
        adrr = td[0].text+':'+td[1].text

        # new_proxy = [adrr,  is_https]
        new_proxy = adrr

        proxies = {
            'http': 'http://'+new_proxy,
        }

        try:
            response = requests.get(url_for_test, headers=headers, proxies=proxies)
            # print response.status_code
            if response.status_code == 200:
                res.append(new_proxy)
        except Exception, e:
            print str(e)
        print i, adrr
        # if i > 5:
        #     break
    # print res
    return res


def getallproxy():
    all_proxy = []
    for i, url in enumerate(PROXY_LIST):
        response = requests.get(url, headers=headers)
        soup = BeautifulSoup(response.content, 'html.parser')
        table_proxy = soup.find("table", {"id": "proxylisttable"})
        tbody = table_proxy.find("tbody")
        all_proxy.append(get_url_from_tbody(tbody))

    # print len(all_proxy[0])
    return (a for a in all_proxy)


def save_proxy(data, name='proxy_list'):
    try:
        with open(name, 'wb') as f:
            pickle.dump(data, f)
    except:
        return None
    return True


def load_proxy(name='proxy_list'):
    try:
        with open(name, 'rb') as f:
            proxy_list = pickle.load(f)
            return proxy_list
    except:
        return {}


if __name__ == '__main__':
    all_p = getallproxy()
    # # a = [1, 2]
    # save_proxy_list(all_p)
    # print get_proxy_list()




