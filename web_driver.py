# -*- coding: 'latin-1' -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from app import application
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import random
from config import DEVICES, USER_AGENTS
import signal


class Webdriver(object):
    isclose = False

    def __init__(self, proxy_http):
        self.screenpath = os.path.join(os.path.dirname(__file__), 'Screenshots')
        # user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        device_user_agent = random.choice(USER_AGENTS)
        print "device_user_agent = {}".format(device_user_agent)
        dcap["phantomjs.page.settings.userAgent"] = (
            random.choice(USER_AGENTS)
        )

        args1 = '--proxy={0}'.format(proxy_http)
        service_args = [
            # '--proxy=185.82.212.95:8080',
            args1,
            '--proxy-type=http',
        ]

        if os.name == 'nt':
            phantomjs_path = os.path.join(os.path.dirname(__file__), 'Resource', 'phantomjs_windows',
                                          'bin', 'phantomjs.exe')
            self.driver = webdriver.PhantomJS(executable_path=phantomjs_path, desired_capabilities=dcap,
                                              service_args=service_args)
            # driver = webdriver.Firefox()

        elif os.name == 'posix':
            service_args.append('--ignore-ssl-errors=true')
            service_args.append('--ssl-protocol=any')
            self.driver = webdriver.PhantomJS(service_args=service_args, desired_capabilities=dcap)

        device = random.choice(DEVICES)
        print 'size_device {}'.format(device)
        self.driver.set_window_size(device[0], device[1])
        timeout = 30
        self.wait = WebDriverWait(self.driver, timeout)
        self.driver.set_page_load_timeout(timeout)
        self.isclose = True

    def kill(self):
        if os.name == 'nt':
            self.driver.quit()
        elif os.name == 'posix':
            try:
                self.driver.service.process.send_signal(signal.SIGTERM)  # kill the specific phantomjs child proc
                self.driver.quit()
                if self.driver.service.process:
                    self.driver.service.process.kill()
            except:
                pass


if __name__ == '__main__':
    print 'test'
    # w = Webdriver()
    # w.kill()
    # # print driver.page_source
    # driver.close()
