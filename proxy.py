import pickle
from datetime import datetime


class Proxy(object):
    def __init__(self, name='proxy_list'):
        try:
            with open(name, 'rb') as f:
                self.proxy_list = pickle.load(f)
                # print self.proxy_list
            if self.proxy_list is None:
                self.proxy_list = {'good': {}, 'bad': [], 'date_updated': datetime.now()}
                return

            if self.proxy_list is not None and self.proxy_list['date_updated'] is not None and\
                            self.proxy_list['date_updated'].date() < datetime.now().date():
                self.proxy_list = {'good': {}, 'bad': [], 'date_updated': datetime.now()}
        except:
            self.proxy_list = {'good': {}, 'bad': [], 'date_updated': datetime.now()}
        finally:
            self.proxy_list['date_updated'] = datetime.now()

    def save(self, name='proxy_list'):
        try:
            with open(name, 'wb') as f:
                pickle.dump(self.proxy_list, f)
        except:
            return False
        return True

    def add_proxy(self, newproxy, type_='good'):
        if type_ == 'good':
            c = self.proxy_list[type_].keys().count(newproxy)
            self.proxy_list[type_][newproxy] = c+1
        else:
            self.proxy_list[type_].append(newproxy)

    def check(self, newproxy):
        if newproxy in self.proxy_list['bad']:
            return False
        return True if self.proxy_list['good'].keys().count(newproxy) < 3 else False


if __name__ == '__main__':
    proxy = Proxy()
